package app

import (
	"basketball/handlers"
	"basketball/storage"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/slham/toolbelt/l"
)

type App struct {
	Router *mux.Router
}

func (a *App) Initialize() bool {
	l.Info(nil, "application initializing")

	ok := l.Initialize(l.Level(os.Getenv("LOG_LEVEL")))
	if !ok {
		l.Error(nil, "failed to initialize logging middleware")
		return false
	}

	ok = storage.Initialize()
	if !ok {
		l.Error(nil, "failed to initialize storage")
		return false
	}

	a.Router = mux.NewRouter()
	a.initializeRoutes()
	l.Info(nil, "Up and Running!")

	return true
}

func (a *App) Run() bool {
	if err := http.ListenAndServe(":"+os.Getenv("RUNTIME_PORT"), cors.Default().Handler(a.Router)); err != nil {
		l.Error(nil, "failed to boot server: %v", err)
		return false
	}
	return true
}

func (a *App) initializeRoutes() {
	a.Router.Use(l.Logging)
	a.Router.Methods("GET").Path("/health").HandlerFunc(handlers.HealthCheck)

	a.Router.Methods("POST").Path("/ratings/{year}").HandlerFunc(handlers.RatePlayers)

	a.Router.Methods("PUT").Path("/players/{year}").HandlerFunc(handlers.StorePlayers)
}
